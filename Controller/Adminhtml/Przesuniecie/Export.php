<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PrzesuniecieMagazynowe\Controller\Adminhtml\Przesuniecie;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class Export extends \Magento\Backend\App\Action
{

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $prodCollFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;


    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $prodCollFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     * @param \Magento\Framework\Filesystem\Io\File $file
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context                     $context,
        Filter                                                  $filter,
        CollectionFactory                                       $prodCollFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface         $productRepository,
        \Magento\Framework\App\Response\Http\FileFactory        $fileFactory,
        \Magento\Framework\Filesystem\DirectoryList             $directoryList,
        \Magento\Framework\Filesystem\Io\File                   $file,
        \Magento\Framework\Controller\Result\RawFactory         $resultRawFactor,
        \Magento\InventoryApi\Api\SourceRepositoryInterface     $sourceRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder            $searchCriteriaBuilder,
        \Magento\InventoryApi\Api\SourceItemRepositoryInterface $sourceItemRepositoryInterface
    )
    {
        $this->filter = $filter;
        $this->prodCollFactory = $prodCollFactory;
        $this->productRepository = $productRepository;
        $this->fileFactory = $fileFactory;
        $this->directoryList = $directoryList;
        $this->file = $file;
        $this->resultRawFactory = $resultRawFactor;
        $this->sourceRepository = $sourceRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sourceItemRepositoryInterface = $sourceItemRepositoryInterface;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->prodCollFactory->create());
        $pm = [];
        $pm[] = $this->addHeaders();
        foreach ($collection->getAllIds() as $productId) {
            $productDataObject = $this->productRepository->getById($productId);

            $pm[] = $this->getSourceItemQtyBySKU($productDataObject);
        }

        $resultRaw = $this->downloadFile($pm);
        $this->messageManager->addSuccess(__('Wygenerowano PM dla %1 produktów.', $collection->getSize()));
        return $resultRaw;


        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('catalog/product/index');

    }

    public function addHeaders()
    {
        $pm = ['name','sku', 'from', 'to', 'qty'];
        $sources = $this->getSourceList();
        foreach ($sources as $source) {
            array_push($pm, $source->getData('source_code'));
        }
        return $pm;
    }

    private function getSourceList(){
        $sourceData = $this->sourceRepository->getList();
        return $sourceData->getItems();
    }

    public function getSourceItemQtyBySKU($product)
    {
        $pm = [$product->getName(), $product->getSku(), '', '', ''];
        $sources = $this->getSourceList();
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(\Magento\InventoryApi\Api\Data\SourceItemInterface::SKU, $product->getSku())
            ->create();

        $result = $this->sourceItemRepositoryInterface->getList($searchCriteria)->getItems();
        foreach ($sources as $source) {

            foreach ($result as $item) {

                // print_r($item->getData());
                /*
                    [source_item_id] => 7
                    [source_code] => default
                    [sku] => 24-UB02
                    [quantity] => 98.0000
                    [status] => 1
                 */


                if (isset($item['source_code']) && $item['source_code'] == $source->getData('source_code')) {
                    $qty = $item['quantity'];
                    break;
                } else {
                    $qty = null;
                }

            }
            array_push($pm, $qty);
        }



        return $pm;
    }

    private function downloadFile($pm)
    {
        $this->getFileName()->saveArrayToCsv($pm);


        $this->fileFactory->create(
            $this->fileName,
            file_get_contents($this->path_to_file),
            \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
        );
        $resultRaw = $this->resultRawFactory->create();
        return $resultRaw;
    }


    private function getFileName()
    {
        $this->var = $this->directoryList->getPath('var');

        if (!file_exists($this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'pm')) {
            $this->file->mkdir($this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'pm', 0775);
        }
        $this->fileName = 'pm_export_' . date('Ymd_His') . '.csv';
        $this->path_to_file = $this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'pm' . DIRECTORY_SEPARATOR . $this->fileName;
        return $this;
    }

    private function saveArrayToCsv($array)
    {
        $fp = fopen($this->path_to_file, 'w');
        foreach ($array as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);
    }


}


<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PrzesuniecieMagazynowe\Model;

use Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface;
use Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Przesuniecie extends \Magento\Framework\Model\AbstractModel
{

    protected $przesuniecieDataFactory;

    protected $_eventPrefix = 'kowal_przesunieciemagazynowe_przesuniecie';
    protected $dataObjectHelper;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PrzesuniecieInterfaceFactory $przesuniecieDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Kowal\PrzesuniecieMagazynowe\Model\ResourceModel\Przesuniecie $resource
     * @param \Kowal\PrzesuniecieMagazynowe\Model\ResourceModel\Przesuniecie\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        PrzesuniecieInterfaceFactory $przesuniecieDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Kowal\PrzesuniecieMagazynowe\Model\ResourceModel\Przesuniecie $resource,
        \Kowal\PrzesuniecieMagazynowe\Model\ResourceModel\Przesuniecie\Collection $resourceCollection,
        array $data = []
    ) {
        $this->przesuniecieDataFactory = $przesuniecieDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve przesuniecie model with przesuniecie data
     * @return PrzesuniecieInterface
     */
    public function getDataModel()
    {
        $przesuniecieData = $this->getData();
        
        $przesuniecieDataObject = $this->przesuniecieDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $przesuniecieDataObject,
            $przesuniecieData,
            PrzesuniecieInterface::class
        );
        
        return $przesuniecieDataObject;
    }
}


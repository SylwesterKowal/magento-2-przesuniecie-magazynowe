<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PrzesuniecieMagazynowe\Model\ResourceModel\Przesuniecie;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'przesuniecie_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Kowal\PrzesuniecieMagazynowe\Model\Przesuniecie::class,
            \Kowal\PrzesuniecieMagazynowe\Model\ResourceModel\Przesuniecie::class
        );
    }
}


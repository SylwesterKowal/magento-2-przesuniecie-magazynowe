<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PrzesuniecieMagazynowe\Model;

use Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterfaceFactory;
use Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsSearchResultsInterfaceFactory;
use Kowal\PrzesuniecieMagazynowe\Api\PrzesuniecieItemsRepositoryInterface;
use Kowal\PrzesuniecieMagazynowe\Model\ResourceModel\PrzesuniecieItems as ResourcePrzesuniecieItems;
use Kowal\PrzesuniecieMagazynowe\Model\ResourceModel\PrzesuniecieItems\CollectionFactory as PrzesuniecieItemsCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class PrzesuniecieItemsRepository implements PrzesuniecieItemsRepositoryInterface
{

    protected $przesuniecieItemsFactory;

    protected $dataObjectHelper;

    protected $extensibleDataObjectConverter;
    private $collectionProcessor;

    private $storeManager;

    protected $searchResultsFactory;

    protected $resource;

    protected $extensionAttributesJoinProcessor;

    protected $dataObjectProcessor;

    protected $dataPrzesuniecieItemsFactory;

    protected $przesuniecieItemsCollectionFactory;


    /**
     * @param ResourcePrzesuniecieItems $resource
     * @param PrzesuniecieItemsFactory $przesuniecieItemsFactory
     * @param PrzesuniecieItemsInterfaceFactory $dataPrzesuniecieItemsFactory
     * @param PrzesuniecieItemsCollectionFactory $przesuniecieItemsCollectionFactory
     * @param PrzesuniecieItemsSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourcePrzesuniecieItems $resource,
        PrzesuniecieItemsFactory $przesuniecieItemsFactory,
        PrzesuniecieItemsInterfaceFactory $dataPrzesuniecieItemsFactory,
        PrzesuniecieItemsCollectionFactory $przesuniecieItemsCollectionFactory,
        PrzesuniecieItemsSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->przesuniecieItemsFactory = $przesuniecieItemsFactory;
        $this->przesuniecieItemsCollectionFactory = $przesuniecieItemsCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPrzesuniecieItemsFactory = $dataPrzesuniecieItemsFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface $przesuniecieItems
    ) {
        /* if (empty($przesuniecieItems->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $przesuniecieItems->setStoreId($storeId);
        } */
        
        $przesuniecieItemsData = $this->extensibleDataObjectConverter->toNestedArray(
            $przesuniecieItems,
            [],
            \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface::class
        );
        
        $przesuniecieItemsModel = $this->przesuniecieItemsFactory->create()->setData($przesuniecieItemsData);
        
        try {
            $this->resource->save($przesuniecieItemsModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the przesuniecieItems: %1',
                $exception->getMessage()
            ));
        }
        return $przesuniecieItemsModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($przesuniecieItemsId)
    {
        $przesuniecieItems = $this->przesuniecieItemsFactory->create();
        $this->resource->load($przesuniecieItems, $przesuniecieItemsId);
        if (!$przesuniecieItems->getId()) {
            throw new NoSuchEntityException(__('PrzesuniecieItems with id "%1" does not exist.', $przesuniecieItemsId));
        }
        return $przesuniecieItems->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->przesuniecieItemsCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface $przesuniecieItems
    ) {
        try {
            $przesuniecieItemsModel = $this->przesuniecieItemsFactory->create();
            $this->resource->load($przesuniecieItemsModel, $przesuniecieItems->getPrzesuniecieitemsId());
            $this->resource->delete($przesuniecieItemsModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the PrzesuniecieItems: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($przesuniecieItemsId)
    {
        return $this->delete($this->get($przesuniecieItemsId));
    }
}


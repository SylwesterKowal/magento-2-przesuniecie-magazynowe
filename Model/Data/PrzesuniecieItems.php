<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PrzesuniecieMagazynowe\Model\Data;

use Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface;

class PrzesuniecieItems extends \Magento\Framework\Api\AbstractExtensibleObject implements PrzesuniecieItemsInterface
{

    /**
     * Get przesuniecieitems_id
     * @return string|null
     */
    public function getPrzesuniecieitemsId()
    {
        return $this->_get(self::PRZESUNIECIEITEMS_ID);
    }

    /**
     * Set przesuniecieitems_id
     * @param string $przesuniecieitemsId
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface
     */
    public function setPrzesuniecieitemsId($przesuniecieitemsId)
    {
        return $this->setData(self::PRZESUNIECIEITEMS_ID, $przesuniecieitemsId);
    }

    /**
     * Get sku
     * @return string|null
     */
    public function getSku()
    {
        return $this->_get(self::SKU);
    }

    /**
     * Set sku
     * @param string $sku
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface
     */
    public function setSku($sku)
    {
        return $this->setData(self::SKU, $sku);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get product_name
     * @return string|null
     */
    public function getProductName()
    {
        return $this->_get(self::PRODUCT_NAME);
    }

    /**
     * Set product_name
     * @param string $productName
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface
     */
    public function setProductName($productName)
    {
        return $this->setData(self::PRODUCT_NAME, $productName);
    }

    /**
     * Get from_source
     * @return string|null
     */
    public function getFromSource()
    {
        return $this->_get(self::FROM_SOURCE);
    }

    /**
     * Set from_source
     * @param string $fromSource
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface
     */
    public function setFromSource($fromSource)
    {
        return $this->setData(self::FROM_SOURCE, $fromSource);
    }

    /**
     * Get to_source
     * @return string|null
     */
    public function getToSource()
    {
        return $this->_get(self::TO_SOURCE);
    }

    /**
     * Set to_source
     * @param string $toSource
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface
     */
    public function setToSource($toSource)
    {
        return $this->setData(self::TO_SOURCE, $toSource);
    }

    /**
     * Get qty
     * @return string|null
     */
    public function getQty()
    {
        return $this->_get(self::QTY);
    }

    /**
     * Set qty
     * @param string $qty
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface
     */
    public function setQty($qty)
    {
        return $this->setData(self::QTY, $qty);
    }
}


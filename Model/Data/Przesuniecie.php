<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PrzesuniecieMagazynowe\Model\Data;

use Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface;

class Przesuniecie extends \Magento\Framework\Api\AbstractExtensibleObject implements PrzesuniecieInterface
{

    /**
     * Get przesuniecie_id
     * @return string|null
     */
    public function getPrzesuniecieId()
    {
        return $this->_get(self::PRZESUNIECIE_ID);
    }

    /**
     * Set przesuniecie_id
     * @param string $przesuniecieId
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface
     */
    public function setPrzesuniecieId($przesuniecieId)
    {
        return $this->setData(self::PRZESUNIECIE_ID, $przesuniecieId);
    }

    /**
     * Get created
     * @return string|null
     */
    public function getCreated()
    {
        return $this->_get(self::CREATED);
    }

    /**
     * Set created
     * @param string $created
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface
     */
    public function setCreated($created)
    {
        return $this->setData(self::CREATED, $created);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get import
     * @return string|null
     */
    public function getImport()
    {
        return $this->_get(self::IMPORT);
    }

    /**
     * Set import
     * @param string $import
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface
     */
    public function setImport($import)
    {
        return $this->setData(self::IMPORT, $import);
    }
}


<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PrzesuniecieMagazynowe\Model;

use Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterfaceFactory;
use Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieSearchResultsInterfaceFactory;
use Kowal\PrzesuniecieMagazynowe\Api\PrzesuniecieRepositoryInterface;
use Kowal\PrzesuniecieMagazynowe\Model\ResourceModel\Przesuniecie as ResourcePrzesuniecie;
use Kowal\PrzesuniecieMagazynowe\Model\ResourceModel\Przesuniecie\CollectionFactory as PrzesuniecieCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class PrzesuniecieRepository implements PrzesuniecieRepositoryInterface
{

    protected $przesuniecieCollectionFactory;

    protected $dataObjectHelper;

    protected $extensibleDataObjectConverter;
    private $collectionProcessor;

    protected $przesuniecieFactory;

    protected $dataPrzesuniecieFactory;

    private $storeManager;

    protected $searchResultsFactory;

    protected $resource;

    protected $extensionAttributesJoinProcessor;

    protected $dataObjectProcessor;


    /**
     * @param ResourcePrzesuniecie $resource
     * @param PrzesuniecieFactory $przesuniecieFactory
     * @param PrzesuniecieInterfaceFactory $dataPrzesuniecieFactory
     * @param PrzesuniecieCollectionFactory $przesuniecieCollectionFactory
     * @param PrzesuniecieSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourcePrzesuniecie $resource,
        PrzesuniecieFactory $przesuniecieFactory,
        PrzesuniecieInterfaceFactory $dataPrzesuniecieFactory,
        PrzesuniecieCollectionFactory $przesuniecieCollectionFactory,
        PrzesuniecieSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->przesuniecieFactory = $przesuniecieFactory;
        $this->przesuniecieCollectionFactory = $przesuniecieCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPrzesuniecieFactory = $dataPrzesuniecieFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface $przesuniecie
    ) {
        /* if (empty($przesuniecie->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $przesuniecie->setStoreId($storeId);
        } */
        
        $przesuniecieData = $this->extensibleDataObjectConverter->toNestedArray(
            $przesuniecie,
            [],
            \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface::class
        );
        
        $przesuniecieModel = $this->przesuniecieFactory->create()->setData($przesuniecieData);
        
        try {
            $this->resource->save($przesuniecieModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the przesuniecie: %1',
                $exception->getMessage()
            ));
        }
        return $przesuniecieModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($przesuniecieId)
    {
        $przesuniecie = $this->przesuniecieFactory->create();
        $this->resource->load($przesuniecie, $przesuniecieId);
        if (!$przesuniecie->getId()) {
            throw new NoSuchEntityException(__('Przesuniecie with id "%1" does not exist.', $przesuniecieId));
        }
        return $przesuniecie->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->przesuniecieCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface $przesuniecie
    ) {
        try {
            $przesuniecieModel = $this->przesuniecieFactory->create();
            $this->resource->load($przesuniecieModel, $przesuniecie->getPrzesuniecieId());
            $this->resource->delete($przesuniecieModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Przesuniecie: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($przesuniecieId)
    {
        return $this->delete($this->get($przesuniecieId));
    }
}


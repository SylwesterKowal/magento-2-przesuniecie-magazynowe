<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PrzesuniecieMagazynowe\Model;

use Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface;
use Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class PrzesuniecieItems extends \Magento\Framework\Model\AbstractModel
{

    protected $przesuniecieitemsDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'kowal_przesunieciemagazynowe_przesuniecieitems';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PrzesuniecieItemsInterfaceFactory $przesuniecieitemsDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Kowal\PrzesuniecieMagazynowe\Model\ResourceModel\PrzesuniecieItems $resource
     * @param \Kowal\PrzesuniecieMagazynowe\Model\ResourceModel\PrzesuniecieItems\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        PrzesuniecieItemsInterfaceFactory $przesuniecieitemsDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Kowal\PrzesuniecieMagazynowe\Model\ResourceModel\PrzesuniecieItems $resource,
        \Kowal\PrzesuniecieMagazynowe\Model\ResourceModel\PrzesuniecieItems\Collection $resourceCollection,
        array $data = []
    ) {
        $this->przesuniecieitemsDataFactory = $przesuniecieitemsDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve przesuniecieitems model with przesuniecieitems data
     * @return PrzesuniecieItemsInterface
     */
    public function getDataModel()
    {
        $przesuniecieitemsData = $this->getData();
        
        $przesuniecieitemsDataObject = $this->przesuniecieitemsDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $przesuniecieitemsDataObject,
            $przesuniecieitemsData,
            PrzesuniecieItemsInterface::class
        );
        
        return $przesuniecieitemsDataObject;
    }
}


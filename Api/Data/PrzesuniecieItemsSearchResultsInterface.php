<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PrzesuniecieMagazynowe\Api\Data;

interface PrzesuniecieItemsSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get PrzesuniecieItems list.
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface[]
     */
    public function getItems();

    /**
     * Set sku list.
     * @param \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}


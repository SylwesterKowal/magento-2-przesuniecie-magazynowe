<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PrzesuniecieMagazynowe\Api\Data;

interface PrzesuniecieInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const CREATED = 'created';
    const PRZESUNIECIE_ID = 'przesuniecie_id';
    const IMPORT = 'import';

    /**
     * Get przesuniecie_id
     * @return string|null
     */
    public function getPrzesuniecieId();

    /**
     * Set przesuniecie_id
     * @param string $przesuniecieId
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface
     */
    public function setPrzesuniecieId($przesuniecieId);

    /**
     * Get created
     * @return string|null
     */
    public function getCreated();

    /**
     * Set created
     * @param string $created
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface
     */
    public function setCreated($created);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieExtensionInterface $extensionAttributes
    );

    /**
     * Get import
     * @return string|null
     */
    public function getImport();

    /**
     * Set import
     * @param string $import
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface
     */
    public function setImport($import);
}


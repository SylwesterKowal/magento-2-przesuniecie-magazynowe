<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PrzesuniecieMagazynowe\Api\Data;

interface PrzesuniecieSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Przesuniecie list.
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface[]
     */
    public function getItems();

    /**
     * Set created list.
     * @param \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}


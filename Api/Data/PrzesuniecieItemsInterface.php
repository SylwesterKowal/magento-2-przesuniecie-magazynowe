<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PrzesuniecieMagazynowe\Api\Data;

interface PrzesuniecieItemsInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const PRODUCT_NAME = 'product_name';
    const QTY = 'qty';
    const PRZESUNIECIEITEMS_ID = 'przesuniecieitems_id';
    const FROM_SOURCE = 'from_source';
    const SKU = 'sku';
    const TO_SOURCE = 'to_source';

    /**
     * Get przesuniecieitems_id
     * @return string|null
     */
    public function getPrzesuniecieitemsId();

    /**
     * Set przesuniecieitems_id
     * @param string $przesuniecieitemsId
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface
     */
    public function setPrzesuniecieitemsId($przesuniecieitemsId);

    /**
     * Get sku
     * @return string|null
     */
    public function getSku();

    /**
     * Set sku
     * @param string $sku
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface
     */
    public function setSku($sku);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsExtensionInterface $extensionAttributes
    );

    /**
     * Get product_name
     * @return string|null
     */
    public function getProductName();

    /**
     * Set product_name
     * @param string $productName
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface
     */
    public function setProductName($productName);

    /**
     * Get from_source
     * @return string|null
     */
    public function getFromSource();

    /**
     * Set from_source
     * @param string $fromSource
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface
     */
    public function setFromSource($fromSource);

    /**
     * Get to_source
     * @return string|null
     */
    public function getToSource();

    /**
     * Set to_source
     * @param string $toSource
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface
     */
    public function setToSource($toSource);

    /**
     * Get qty
     * @return string|null
     */
    public function getQty();

    /**
     * Set qty
     * @param string $qty
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface
     */
    public function setQty($qty);
}


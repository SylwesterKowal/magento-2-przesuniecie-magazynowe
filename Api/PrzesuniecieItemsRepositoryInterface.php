<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PrzesuniecieMagazynowe\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface PrzesuniecieItemsRepositoryInterface
{

    /**
     * Save PrzesuniecieItems
     * @param \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface $przesuniecieItems
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface $przesuniecieItems
    );

    /**
     * Retrieve PrzesuniecieItems
     * @param string $przesuniecieitemsId
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($przesuniecieitemsId);

    /**
     * Retrieve PrzesuniecieItems matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete PrzesuniecieItems
     * @param \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface $przesuniecieItems
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieItemsInterface $przesuniecieItems
    );

    /**
     * Delete PrzesuniecieItems by ID
     * @param string $przesuniecieitemsId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($przesuniecieitemsId);
}


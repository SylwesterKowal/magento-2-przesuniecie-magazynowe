<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PrzesuniecieMagazynowe\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface PrzesuniecieRepositoryInterface
{

    /**
     * Save Przesuniecie
     * @param \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface $przesuniecie
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface $przesuniecie
    );

    /**
     * Retrieve Przesuniecie
     * @param string $przesuniecieId
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($przesuniecieId);

    /**
     * Retrieve Przesuniecie matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Przesuniecie
     * @param \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface $przesuniecie
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\PrzesuniecieMagazynowe\Api\Data\PrzesuniecieInterface $przesuniecie
    );

    /**
     * Delete Przesuniecie by ID
     * @param string $przesuniecieId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($przesuniecieId);
}


# Mage2 Module Kowal PrzesuniecieMagazynowe

    ``kowal/module-przesunieciemagazynowe``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Przesunięcie magazynowe

## Installation

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_PrzesuniecieMagazynowe`
 - Apply database updates by running `php bin/magento setup:upgrade --keep-generated`
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-przesunieciemagazynowe`
 - enable the module by running `php bin/magento module:enable Kowal_PrzesuniecieMagazynowe`
 - apply database updates by running `php bin/magento setup:upgrade --keep-generated`
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration




## Specifications

 - Controller
	- adminhtml > kowal_przesunieciemagazynowe/przesuniecie/export


## Attributes



